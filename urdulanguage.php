<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
// 
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

// The original strings (English) are case-sensitive.

/* Note for translators about translation of text ending with punctuation
 *
 * The current list of concerned punctuation can be found in 'lib/init/tra.php'
 * On 2009-03-02, it is: (':', '!', ';', '.', ',', '?')
 * For clarity, we explain here only for colons: ':' but it is the same for the rest
 *
 * Short version: it is not a problem that string "Login:" has no translation. Only "Login" needs to be translated.
 *
 * Technical justification:
 * If a string ending with colon needs translating (like "{tr}Login:{/tr}")
 * then Tiki tries to translate 'Login' and ':' separately.
 * This allows to have only one translation for "{tr}Login{/tr}" and "{tr}Login:{/tr}"
 * and it still allows to translate ":" as " :" for languages that
 * need it (like French)
 * Note: the difference is invisible but " :" has an UTF-8 non-breaking-space, not a regular space, but the UTF-8 equivalent of the HTML &nbsp;.
 * This allows correctly displaying emails and JavaScript messages, not only web pages as would happen with &nbsp;.
 */
include('lang/en/language.php'); // Needed for providing a sensible default text for untranslated strings with context like : "edit_C(verb)"
$lang_current = array(
"File %0 succesfully imported" => " مسل ٪0 کامیابی سے درآمد ",
"category defaults" => " زمرہ نادہندگان ",
"You must specify a directory" => "&#1672;&#1575;&#1574;&#1585;&#1705;&#1657;&#1585;&#1740; &#1705;&#1575; &#1575;&#1606;&#1578;&#1582;&#1575;&#1576; &#1705;&#1585;&#1740;&#1722;",
" stalled files will attempt to OCR again" => "دوبارہ کوشش کریں گی کی OCR  رکی ہوئی فائلیں ",
"The search text was reindexed for a total of %0 files" => " تلاش متن کو کل ٪0 مسلوں کے لیے دوبارہ اشاریہ کیا گیا ",
"Was not able to change status of OCR files from stalled to pending" => " فائلوں کی حالت تعطل سے زیر التوا میں تبدیل کرنے کے قابل نہیں تھا OCR ",
"Tags successfully cleaned up" => " کامیابی سے صاف Tags ",
"Tag cleanup failed" => " صفائی ناکامTag  ",
"Orphan images successfully removed" => " یتیم تصاویر کامیابی سے ہٹا دی گئیں ",
"moved %d images, %d errors occurred" => "٪د>#1578;#1589;#1575;&#1608;#1740;#1585; &#1705;#1608; &#1605;#1606;#1578;&#1602;#1604; >#1705;#1585;>#1740;#1575; &#1711;#1740;>#1575; &#1729;&#1746; ٪د، ٪ڈی غلطیاں ہوئیں ",
"a timeout occurred. Hit the reload button to move the rest" => " ایک ٹائم آؤٹ ہوا۔ باقی کو منتقل کرنے کے لئے ری لوڈ بٹن ہٹائیں ",
"Tiki Email Test" => ٹکی ای میل ٹیسٹ
"Tiki Test email from" => " ٹکی ٹیسٹ ای میل از ",
"Unable to send mail" => " ڈاک بھیجنے میں ناکام ",
"Test mail sent to" => " ٹیسٹ ڈاک کو بھیجا گیا ",
"registration choices" => " رجسٹریشن کے انتخاب ",
"This method of captcha is not supported by your server, please select another or upgrade" => " کیپچا کا یہ طریقہ آپ کے سرور کی معاونت سے نہیں ہے، براہ کرم دوسرا منتخب کریں یا اپ گریڈ کریں ",
"You need to select a file to upload" => " اپ لوڈ کرنے کے لیے آپ کو مسل منتخب کرنے کی ضرورت ہے ",
"File Upload Error: %0" => " مسل اپ لوڈ نقص: ٪0",
"Uploaded file has been populated into database and indexed. Ready to generate password lists" => " اپ لوڈ کی گئی فائل کو ڈیٹا بیس میں آباد کیا گیا ہے اور اشاریہ کیا گیا ہے۔ پاس ورڈ فہرستیں تیار کرنے کے لیے تیار ",
"Unable to Write Password File to Disk" => " ڈسک پر پاس ورڈ مسل نہیں لکھ سکا ",
"%d users were assigned to groups based on user emails matching the patterns defined for the groups" => "ڈی صارفین کو گروپس کے لیے متعین کردہ نمونوں سے مشابہ صارف ای میلز کی بنیاد پر گروپس کو تفویض کیا گیا تھا ٪",
"No user emails matched the group pattern definitions, or the matching users were already assigned, or email patterns have not been set for any groups" => " کوئی صارف ای میل گروپ پیٹرن تعریفوں سے میل نہیں کھاتا، یا مشابہ صارفین پہلے ہی تفویض کیے گئے تھے، یا کسی گروپ کے لیے ای میل پیٹرن سیٹ نہیں کیے گئے ہیں ",
"%0 tracker(s) with %1 item(s) were synchronized" => "ٹریکر(س) مع ٪1 شے(س) ہم وقت سازی کی گئی ٪0  ",
"None" => " کوئی ",
"composer.lock file was removed" => "  فائل ہٹا دی گئی composer.lock",
"composer.lock file is not writable, so it can not be removed" => " قابل تحریر نہیں ہے، لہذا اسے ہٹایا نہیں جا سکتا  .composer.lockموسیقار",
"composer.lock file do not exists" => "  فائل موجود نہیں composer.lock",
"Vendor folder contents was removed" => " وینڈر پوشہ مشمولات ہٹا دیے گئے ",
"Vendor folder is not writable" => " وینڈر پوشہ قابل تحریر نہیں ",
"Vendor folder do not exists" => " وینڈر پوشہ موجود نہیں ",
"Used" => " استعمال ",
"Available" => " دست یاب ",
"Hit" => " ٹکر ",
"Miss" => " یاد ",
"An error occurred: " => " ایک نقص واقع ہوا:",
"profile applied" => " پروفائل لاگو ",
"The following list of changes has been applied" => "مندرجہ ذیل تبدیلیاں کی فہرست کا اطلاق کیا گیا ہے",
"Profile forgotten" => " پروفائل بھول گیا ",
"New rating configuration %0 created" => " نئی درجہ بندی تشکیل ٪0 بنائی گئی ",
"Rating configuration updated for %0" => " درجہ بندی تشکیل برائے ٪0 تازہ کاری ",
"Scoring events replaced with form data" => " اسکورنگ واقعات کو فارم ڈیٹا سے بدل دیا گیا ",
"Unable to get info from log file" => " لاگ فائل سے معلومات حاصل کرنے میں ناکام ",
"You must set a home file gallery" => " آپ کو گھر مسل گیلری سیٹ کرنا ضروری ہے ",
"Tag already exists" => " پہلے سے موجود ہے tag ",
"Tag %0 created" => " بنایا گیا tag ٪0",
"Tag %0 not found" => " نہیں ملا tag ٪0",
"Tag %0 restored" => "  بحال tag  ٪0",
"Tag %0 not restored" => "بحال نہیں tag  ٪0",
"Tag %0 removed" => "ہٹا دیا گیا tag ٪0  ",
"Tag %0 not removed" => " نہیں ہٹایا گیا tag ٪0",
"Process to remove pictures has completed" => " تصاویر ہٹانے کا عمل مکمل ہو چکا ہے ",
"Dump created at %0" => " ڈمپ ٪0 پر بنایا گیا ",
"Dump was not created. Please check permissions for the storage/ directory" => " ڈمپ نہیں بنایا گیا تھا. براہ کرم سٹوریج/ ڈائریکٹری کے لیے اجازتیں چیک کریں ",
"Dump file %0 removed" => " ڈمپ مسل ٪0 ہٹا دی گئی ",
"Dump file %0 was not removed" => " ڈمپ مسل ٪0 ہٹایا نہیں گیا ",
"No update was made" => " کوئی اپ ڈیٹ نہیں کی گئی ",
"Areas were updated" => " علاقوں کو اپ ڈیٹ کیا گیا ",
"Welsh" => " ویلش ",

);
$lang = array_replace($lang, $lang_current);

